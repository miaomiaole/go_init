package opt_generics

import (
	"fmt"
	"testing"
)

func Test_sumIntOrFloats(t *testing.T) {
	m := make(map[string]int)
	m["sre"] = 1
	m["sr2"] = 1
	m["sr3"] = 1
	m["sr4"] = 13
	delete(m, "sre")
	fmt.Println("=====", sumIntOrFloats(m))
}
