package opt_algorithms

import (
	"container/list"
	"fmt"
)

type LFU struct {
	len     int // length
	cap     int // capacity
	minFreq int // The element that operates least frequently in LFU

	// key: key of element, value: value of element
	itemMap map[string]*list.Element

	// key: frequency of possible occurrences of all elements in the itemMap
	// value: elements with the same frequency
	freqMap map[int]*list.List
}

func opt_list() {
	list := list.New()
	for i := 0; i < 10; i++ {
		list.PushBack(10 - i)
		list.PushFront(i)
	}
	//遍历链表
	for e := list.Front(); e != nil; e = e.Next() {
		fmt.Println(e.Value)
	}

}
