package opt_interface

import (
	"fmt"
	"math"
)

// 定义接口方法 area()
type shape interface {
	area() float64
}

type square struct {
	side float64
}

type circle struct {
	radius float64
}

// square 方法实现 area()
func (z square) area() float64 {
	return z.side * z.side
}

func (z circle) area() float64 {
	return math.Pi * z.radius * z.radius
}

// 使用接口传参
func info(z shape) {
	fmt.Println(z)
	fmt.Println("area is ", z.area())
}

func totalArea(shapes ...shape) (area float64) {
	for _, value := range shapes {
		area += value.area()
	}
	return area
}
