package opt_interface

import (
	"fmt"
	"testing"
)

func Test_interface(t *testing.T) {
	var s shape = square{10}
	c := circle{2.4}
	fmt.Printf("%T\n", s)
	info(s)
	info(c)

	fmt.Println("total area is ", totalArea(s, c))
}
