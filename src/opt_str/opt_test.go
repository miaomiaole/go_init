package opt_str

import (
	"errors"
	"fmt"
	"io"
	"testing"
)

func TestUpdate_str(t *testing.T) {
	Traver_sal_string()
}

func TestFloatTransDouble(t *testing.T) {
	FloatTransDouble()
}

func TestArray_opt(t *testing.T) {
	Array_opt()
}

func TestSumArr(t *testing.T) {
	SumArr([10]int{33, 55, 67})
}

func TestGenerate_rand(t *testing.T) {
	Generate_rand()
}

func TestSlice_Arr(t *testing.T) {
	Slice_Arr()
}

func TestInit_Scli(t *testing.T) {
	Init_Slice()
}

func Test_Prt_init(t *testing.T) {
	Prt_init()
}

func Test_Struct_Type(t *testing.T) {
	Struct_Type()
}

func TestStruct_person(t *testing.T) {
	structPerson := Struct_person()
	fmt.Println(structPerson)
}

func Test_MarshalJSON(t *testing.T) {
	MarshalJSON()
}

func Test_AonymousFun(t *testing.T) {
	f := 1.3
	f1 := 1.1
	f2 := 1.2
	f3 := 1.4
	AnonymousFun(&f, &f1, &f2, &f3)

	AnonymousFun2()
}

func Test_DeferFun(t *testing.T) {
	defer_fun()
}

func Test_PanicFun(t *testing.T) {
	panic_fun()

}

func Test_Error_fun(t *testing.T) {
	err := error_fun()
	if errors.Is(err, io.EOF) {
		fmt.Println("====== 判断错误是否 err ", err)

	} else if errors.Is(err, errors.New("integer divide by zero ")) { // 对象指针不同
		fmt.Println("====== 判断错误是否 New ", err.Error())
	}
}

func Test_time_fun(t *testing.T) {
	time_fun()
}

func Test_Out_in_str(t *testing.T) {
	out_in_str()
}

func Test_Scan_keyboar(t *testing.T) {
	scan_keyboard()
}

func Test_opt_log_read_write(t *testing.T) {

	opt_logger()
}

func Test_opt_uri(t *testing.T) {
	opt_uri()
}
