package opt_mem

import (
	"bytes"
	"log"
	"math/rand"
	"runtime"
)

// 打印内存使用状态
func readMemStatus() {
	var ms runtime.MemStats
	runtime.ReadMemStats(&ms)
	log.Printf(" ===> Alloc:%d(bytes) HeapIdle:%d(bytes) HeapReleased:%d(bytes)", ms.Alloc, ms.HeapIdle, ms.HeapReleased)
}

func useMem() {
	container := make([]int, 8)
	log.Println("=====> loop begin.")

	for i := 0; i < 32*1000*1000; i++ {
		container = append(container, i)
		if i == 16*1000*1000 {
			readMemStatus()
		}
	}

	log.Println("=====> loop end.")
}

func useCpu() {
	log.Println("====> loop begin.")
	for i := 0; i < 1000; i++ {
		log.Println(genSomeBytes())
	}
	log.Println("======> loop end.")
}

func genSomeBytes() *bytes.Buffer {
	var buff bytes.Buffer
	for i := 0; i < 20000; i++ {
		buff.Write([]byte{'0' + byte(rand.Intn(10))})
	}
	return &buff
}
