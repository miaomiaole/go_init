package opt_mem

import (
	"log"
	"net/http"
	_ "net/http/pprof"
	"runtime"
	"sync"
	"testing"
	"time"
)

func Test_opt_mem(t *testing.T) {
	log.Println("======> [Start].")

	readMemStatus()
	useMem()
	readMemStatus()

	log.Println("======> force gc")
	runtime.GC()
	log.Println("======> [Done].")

	readMemStatus()

	go func() {
		for {
			readMemStatus()
			time.Sleep(10 * time.Second)
		}
	}()

	time.Sleep(3600 * time.Second)
	log.Println("=====> [End]")

}

var wg sync.WaitGroup

func Test_use_cpu(t *testing.T) {
	wg.Add(1)
	go func() {
		for {
			useCpu()
			time.Sleep(time.Second * 1)
		}
	}()

	go func() {
		log.Println(http.ListenAndServe("0.0.0.0:10000", nil))
	}()

	wg.Wait()

}
