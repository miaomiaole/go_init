package opt_context

import (
	"context"
	"testing"
	"time"
)

func Test_go_context(t *testing.T) {
	go_context_cancel()
}

func Test_go_context_deadline(t *testing.T) {
	go_context_with_deadline()
}

func Test_go_context_with_value(t *testing.T) {
	go_context_with_value()
}

func Test_go_context_1(t *testing.T) {
	timeout, _ := context.WithTimeout(context.Background(), 5*time.Second)
	go handle_req(timeout)
	time.Sleep(10 * time.Second)
}
