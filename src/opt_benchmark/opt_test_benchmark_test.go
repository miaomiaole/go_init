package opt_benchmark_test

import (
	"go_init/src/opt_benchmark"
	"testing"
)

func Benchmark_make_slice_without_alloc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		opt_benchmark.Make_slice_without_alloc()
	}
}
func Benchmark_make_slice_prt_alloc(b *testing.B) {
	for i := 0; i < b.N; i++ {
		opt_benchmark.Make_slice_with_prt_alloc()
	}
}
