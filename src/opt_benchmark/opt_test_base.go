package opt_benchmark

// 直接返回数组（需要拷贝）
func Make_slice_without_alloc() []int {
	var new_slice []int
	for i := 0; i < 1000000; i++ {
		new_slice = append(new_slice, i)
	}
	return new_slice
}

func Make_slice_with_prt_alloc() *[]int {
	ints := make([]int, 0, 1000000)
	for i := 0; i < 1000000; i++ {
		ints = append(ints, i)
	}
	return &ints
}
