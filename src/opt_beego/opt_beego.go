package opt_beego

import (
	beego "github.com/beego/beego/v2/server/web"
	_ "go_init/src/opt_beego/routers"
)

func opt_beego_main() {
	beego.Run()
}
