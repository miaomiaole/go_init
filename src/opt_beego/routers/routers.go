package routers

import (
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/context"
	"go_init/src/opt_beego/controllers"
)

func init() {
	beego.Router("/", &controllers.BeegoController{})

	// 直接路由
	beego.Get("/v1", func(ctx *context.Context) {
		ctx.Output.JSON(`{"abc":1}`, true, true)
	})

}
