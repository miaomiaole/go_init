package controllers

import (
	"encoding/json"
	beego "github.com/beego/beego/v2/server/web"
)

type BeegoController struct {
	beego.Controller
}
type person struct {
	Name, Address string
}

// 重写 beego.Controller 的get方法
func (bee *BeegoController) Get() {

	p := person{
		"miaomiaole",
		"time",
	}
	marshal, err := json.Marshal(p)
	if err != nil {
		bee.Ctx.Resp(err)
		return
	}

	writer := bee.Ctx.ResponseWriter
	writer.Status = 201
	header := writer.Header()
	header.Set("Content-Type", " application/json")
	writer.Write(marshal)
}
