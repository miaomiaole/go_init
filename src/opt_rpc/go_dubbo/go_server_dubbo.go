package go_dubbo

// go-dubbo 泛化调用（不依赖接口及类），依赖类转为map

import (
	"context"
	"go_init/src/opt_rpc/go_dubbo/api"
)

import (
	"dubbo.apache.org/dubbo-go/v3/config"
	_ "dubbo.apache.org/dubbo-go/v3/imports"
	"github.com/dubbogo/gost/log/logger"
)

type GreeterProvider struct {
	api.UnimplementedGreeterServer
}

func (s *GreeterProvider) SayHello(ctx context.Context, in *api.HelloRequest) (*api.User, error) {
	logger.Infof("Dubbo3 GreeterProvider get user name = %s\n", in.Name)
	return &api.User{Name: "Hello " + in.Name, Id: "12345", Age: 21}, nil
}

type Register struct {
	api.UnimplementedUserImplServer
}

func (this *Register) RegisterUser(ctx context.Context, in *api.User) (*api.User, error) {
	return &api.User{
		Name: "======",
		Id:   "234545",
		Age:  2,
	}, nil
}

// 注册模式：Remote（nacos，Eecd,zk),local:直连
// 注册中心配置（记录提供/消费者元数据）
// 接口（服务）proto中定义的：service
// 注册信息（应用名，接口列表、元数据信息，IP地址）
// 元数据：接口名、包含的方法、方法对应的参数、序列化方式、协议
var registryType = config.NewRegistryConfigBuilder().
	SetTimeout("3s").
	SetProtocol("zookeeper").
	SetAddress("127.0.0.1:2181").
	SetGroup("myGroup").
	SetRegistryType("interface").Build()

// 远程调用协议 RPC(tri,dubbo,gRPC)：TCP报文的封装和拆解过程
// 序列化协议：protobuf，hessian2，msgpack，自定义
var protocolConfig = config.NewProtocolConfigBuilder().
	SetPort("20000").
	SetName("tri").
	Build()

// 应用级别组件
// App information ：应用名、版本号、数据上报方式
// Consumer: Reference 远程结构的引用
// Provider: Service 暴露服务
// Registry
// Protocol:（只存在服务端）
// 元数据
// 路由
// 日志
// 监控
func dubbo_server() {

	// 暴露服务
	config.SetProviderService(&GreeterProvider{})

	// 使用配置
	rootConfig := config.NewRootConfigBuilder().AddRegistry("zk", registryType).
		AddProtocol("triple-protocol-id", protocolConfig). // add protocol, key is custom
		Build()

	if err := config.Load(config.WithRootConfig(rootConfig)); err != nil {
		panic(err)
	}

	logger.Infof("start to dubbo service")
	select {}
}

func dubbo_client() {

	var grpcGreeterImpl = new(api.GreeterClientImpl)

	config.SetConsumerService(grpcGreeterImpl)

	rootConfig := config.NewRootConfigBuilder().AddRegistry("zk", registryType).
		Build()

	if err := config.Load(config.WithRootConfig(rootConfig)); err != nil {
		panic(err)
	}

	logger.Infof("start to dubbo client")

	req := &api.HelloRequest{
		Name: "laurence",
	}
	hello, err := grpcGreeterImpl.SayHello(context.Background(), req)
	if err != nil {
		logger.Error(err)
	}
	logger.Infof("client response result :%v\n", hello)
}
