package go_rpc

import (
	"fmt"
	"log"
	"net/http"
	"net/rpc"
)

type Params struct {
	Width, Height float32
}

type Rect struct {
}

func (r *Rect) Area(p Params, ret *float32) error {
	fmt.Println("=====remote======")
	*ret = p.Width * p.Width
	return nil
}

// 周长
func (r *Rect) Perimeter(p Params, ret *float32) error {
	fmt.Println("=====remote======")
	*ret = (p.Height + p.Width) * 2
	return nil
}

func rpc_server() {
	// rect 方法注册到http 8000 端口上
	rpc.Register(&Rect{})
	rpc.HandleHTTP()
	err := http.ListenAndServe(":8000", nil)
	if err != nil {
		log.Println("rpc listen 8000 err", err)
	}
}

func rpc_client() {
	dialHTTP, err := rpc.DialHTTP("tcp", ":8000")
	if err != nil {
		log.Panicln("connect 8000 err ", err)
	}

	var ret float32
	dialHTTP.Call("Rect.Perimeter", Params{5.2, 10.5}, &ret)

	fmt.Println("remote method rect.Perimeter()", ret)
}
