package opt_reflect

import (
	"fmt"
	"testing"
)

func Test_reflect_type(t *testing.T) {
	reflect_type(23)

	//reflect_type("23")
}

func Test_reflect_value(t *testing.T) {
	reflect_value(1.4)
}

func Test_reflect_struct(t *testing.T) {
	user := User{
		123,
		"ster",
		12,
		func(a int) {
			fmt.Println("a is ", a)
		},
	}
	user.SetAge()
	reflect_struct(user)
}

func Test_reflect_struct_2(t *testing.T) {
	reflect_struct_2()
}
