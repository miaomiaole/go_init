package opt_gorm_gen

import (
	"gorm.io/driver/postgres"
	"gorm.io/gen"
	"gorm.io/gorm"
)

// 生成查询SQL
type Querier interface {
	// SELECT * FROM @@table WHERE name = @name{{if role !=""}} AND role = @role{{end}}
	FilterWithNameAndRole(name, role string) ([]gen.T, error)
}

var dsn = "host=43.139.126.192 user=root password=10086 dbname=initdb port=7432 sslmode=disable TimeZone=Asia/Shanghai"
var db, _ = gorm.Open(postgres.Open(dsn), &gorm.Config{})

func Generate_Postgress() {
	generator := gen.NewGenerator(gen.Config{
		OutPath: "D:\\code\\go_init\\src\\opt_gorm",
		Mode:    gen.WithoutContext | gen.WithDefaultQuery | gen.WithQueryInterface,
	})

	generator.UseDB(db)

	generator.ApplyBasic(User{})

	generator.ApplyInterface(func(Querier) {}, User{}, Company{})

	generator.Execute()
}

type User struct {
}

type Company struct {
}

// gorm_gen 代码生产
func gorm_gen() {
	generator := gen.NewGenerator(gen.Config{
		OutPath: "model",
		Mode:    gen.WithoutContext | gen.WithDefaultQuery | gen.WithQueryInterface,
	})
	generator.UseDB(db)

	generator.GenerateAllTable()

	generator.Execute()

}
