package opt_excel

import (
	"fmt"
	"github.com/xuri/excelize/v2"
)

func create_file() {
	file := excelize.NewFile()
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	index, err := file.NewSheet("Sheet2")

	if err != nil {
		fmt.Println(err)
		return
	}
	// 设置单元格的值
	file.SetCellValue("Sheet2", "A2", "Hello world.")
	file.SetCellValue("Sheet1", "B2", 100)
	// 设置工作簿的默认工作表
	file.SetActiveSheet(index)
	// 根据指定路径保存文件
	if err := file.SaveAs("Book1.xlsx"); err != nil {
		fmt.Println(err)
	}
}
