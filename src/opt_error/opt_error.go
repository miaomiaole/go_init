package opt_error

import (
	"errors"
	"fmt"
)

func create_err() {
	err := errors.New("no found static")

	err2 := fmt.Errorf("some context : %v", err)

	err3 := fmt.Errorf("some context :%w", err)

	// false
	if errors.Is(err, err2) {
		fmt.Println("err2 unpack err")
	}

	//false
	if errors.Is(err2, err) {
		fmt.Println("err unpack err2")
	}

	fmt.Println("=====", err3)
	fmt.Println("=====", err, "====", err2, "=====", err3)
}
