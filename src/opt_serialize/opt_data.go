package opt_serialize

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"os"
	"time"
)

type Person struct {
	Name, Hobby string
	Age         uint
}

type Person2 struct {
	Name     string    `json:"name"`
	Hobby    string    `json:"hobby"`
	Age      uint      `json:"age"`
	Birthday time.Time `json:"birthday"`
}

func user_person() {
	p := Person{"person", "hello", 2}
	marshal, err := json.Marshal(p)
	if err != nil {
		fmt.Println("json Marshal err", err)
	}
	fmt.Println(string(marshal))

	indent, _ := json.MarshalIndent(p, "", "  ")

	fmt.Println("MarshalIndent ", string(indent))

	person := Person{}
	err = json.Unmarshal(marshal, &person)
	if err != nil {
		fmt.Println("Unmarshal is err ", err)
	}

	fmt.Println(person.Hobby)

	fmt.Println("=====================")

	person2 := Person2{
		"person2",
		"hobby",
		2,
		time.Now(),
	}
	bytes, err := json.Marshal(&person2)
	if err != nil {
		fmt.Println("person2 is err", err)
	}
	fmt.Println(string(bytes))

	strPerson2 := []byte(`{"name":"person2",
           "hobby":"hobby",
          "age":2,
           "birthday":"2023-11-09 17:38:29"}`)

	p2 := Person2{}
	err = json.Unmarshal(strPerson2, &p2)
	if err != nil {
		fmt.Println("==========Unmarshal  err", err)
	}
	fmt.Println("err is ", p2)
}

type Cache struct {
	CacheConfig         string `xml:"CacheConfig.id"`
	CacheMethod         string `xml:"cacheMethod"`
	CacheIP             string `xml:"cacheIP"`
	PoolConfigMaxActive int    `xml:"poolConfigMaxActive"`
	PoolConfigMaxIdle   int    `xml:"poolConfigMaxIdle"`
	PoolConfigMaxWait   int    `xml:"poolConfigMaxWait"`
	JedisPoolTimeOut    int    `xml:"jedisPoolTimeOut"`
	Password            string `xml:"password"`
}

type Caches struct {
	Caches []Cache `xml:"CacheConfigs"`
}

func parsing_xml() {
	file, err := os.ReadFile("./cache.xml")
	if err != nil {
		fmt.Println("xml read err ,", err)
		return
	}
	caches := &Caches{}
	err = xml.Unmarshal(file, caches)
	if err != nil {
		fmt.Println("xml Unmarshal err,", err)
	}
	fmt.Printf("xml: %#v\n", caches)
}
