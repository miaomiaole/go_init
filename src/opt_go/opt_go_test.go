package opt_go

import (
	"fmt"
	"testing"
)

func Test_execute(t *testing.T) {
	execute_run()
}
func Test_execute_incrementor(t *testing.T) {
	excute_incrementor()
}

func Test_chin_go(t *testing.T) {
	chin_go()
}

func Test_chin_go2(t *testing.T) {
	// error panic: send on closed channel（wg为0时，chanel关闭）
	chin_go_2()
}

func Test_chin_go_3(t *testing.T) {
	for i := 0; i < 1000; i++ {
		chin_go_3()
	}
}

func Test_chin_go_4(t *testing.T) {
	chin_go_4()
}

func Test_chin_go_5(t *testing.T) {
	chin_go_5()
}

func Test_chin_go_6(t *testing.T) {
	chin_go_6()
}

func Test_chin_go_7(t *testing.T) {
	factorial1 := factorial_1(4)
	for i := range factorial1 {
		fmt.Println(i)
	}
}

func Test_chin_go_8(t *testing.T) {
	out := gen(2, 4, 5, 6, 78, 1)
	float64s := sq(out)
	for f := range float64s {
		fmt.Println(f)
	}

}

func Test_go_chan_go_9(t *testing.T) {
	go_chan_go_9()
}

func Test_go_sched(t *testing.T) {
	for i := 0; i < 100; i++ {
		go_sched()
	}
}

func Test_go_time_1(t *testing.T) {
	timer_go_1()
	go_chan_cache()
}

func Test_go_ticker(t *testing.T) {
	ticker_new()
}

func Test_run_once(t *testing.T) {
	run_once()
}

func Test_syncMap(t *testing.T) {
	syncMap()
}
